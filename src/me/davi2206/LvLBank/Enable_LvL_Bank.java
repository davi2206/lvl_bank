package me.davi2206.LvLBank;

import managers.BankManagement;
import managers.BankYML;
import managers.CommandTabCompleter;
import managers.FileSaveTimer;
import managers.PlayerCommands;
import managers.SignManager;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import creators.GenerateFiles;

public class Enable_LvL_Bank extends JavaPlugin implements Listener
{
	private SignManager signManager;
	private BankManagement bm;
	private GenerateFiles genFiles;
	private FileSaveTimer fst;
	
	private ConsoleCommandSender clog;
	private PlayerCommands pCmds;
	
	private String configVersion = "1.2";
	
	@Override
	public void onEnable()
	{
		clog = this.getServer().getConsoleSender();
		
		genFiles = new GenerateFiles(this);
		genFiles.generateConfig(configVersion);
		genFiles.generateChangelog();

		bm = getBankManager();
		
		signManager = SignManager.getInstance(bm, this);
		
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getServer().getPluginManager().registerEvents(signManager, this);
		
		pCmds = PlayerCommands.getInstance(this);
		pCmds.checkMinMaxValues();
		
		this.getCommand("lvlBank").setExecutor(pCmds);
		getCommand("lvlBank").setTabCompleter(CommandTabCompleter.getInstance(bm));
		
		clog.sendMessage("LvL Bank Enabeled!");
	}
	
	public BankManagement getBankManager()
	{
		BankManagement bank = BankYML.getInstance(this);
		fst = new FileSaveTimer(bank);
		fst.runTaskTimer(this, 0, (5 * (60 * 20)));
		return bank;
	}
	
	public void onDisable()
	{
		bm.save();

		clog.sendMessage("LvL Bank Disabling \n");

		signManager = null;
		bm = null;
		genFiles = null;
		fst = null;
		clog = null;
		HandlerList.unregisterAll();
	}
}
