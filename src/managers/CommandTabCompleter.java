package managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class CommandTabCompleter implements TabExecutor
{
	private BankManagement bm;
	private static CommandTabCompleter completer;

	private CommandTabCompleter(BankManagement bank)
	{
		bm = bank;
	}

	public static CommandTabCompleter getInstance(BankManagement bank)
	{
		if (completer == null)
		{
			completer = new CommandTabCompleter(bank);
		}
		return completer;
	}

	// XXX Handle Tab completion of commands
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd,
			String lable, String[] args)
	{
		List<String> result = new ArrayList<>();
		
		//List<String> result = new ArrayList<>();
		if(sender instanceof Player) 
		{
			if(args.length == 1)
			{
				ArrayList<String> tmp = new ArrayList<String>();
				
				tmp.add("limits");
				tmp.add("help");
				tmp.add("deposit");
				tmp.add("withdraw");
				tmp.add("balance");
				
				
				for(String s : tmp) 
				{
					if(s.contains(args[0]))
					{
						result.add(s);
					}
				}
			}
			else if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase("balance"))
				{
					//ArrayList<String> tmp2 = new ArrayList<String>();
					for(World w : Bukkit.getWorlds()) 
					{
						String name = w.getName();
						String group = bm.getGroup(name);						
						
						if(w.getName().toLowerCase().contains(args[1].toLowerCase()) || !group.equals("Excluded_Worlds"))
						{
							result.add(w.getName());
						}
					}
				}
			}
			else return null;
		}
		else
		{
			result.add("reload");
			result.add("limits");
			result.add("help");
		}
		
		return result;
	}

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
			String[] arg3)
	{
		// TODO Auto-generated method stub
		return false;
	}
}














